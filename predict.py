from keras.models import load_model
import matplotlib.image as mpimg
import numpy as np
import os

model = load_model('model.hd5')

def imgprob(filename):
    img = mpimg.imread(filename)
    img = img[:,:,0]
    img.shape = [1,800,800,1] #need to do this to feed it into the model
    img = img.astype('float32')
    p = model.predict(img, verbose = 1)
    return p

def predictimg(filename):
    p = imgprob(filename)
    predicted_class_y = np.argmax(p)
    return predicted_class_y

def list_test_files(folder):
    test_files = []
    for dirname, dirnames, filenames in os.walk(folder):
        for filename in filenames:
            test_files.append(os.path.join(dirname, filename))
    return(test_files)

legenda = ["Regular", "Jellyfish"]

#take images from a (test) folder and predict their class
def predict_on_folder(folder):
    cerchiati = 0
    noncerchiati = 0 
    for test_file in list_test_files(folder):
        predicted = predictimg(test_file)
        noncerchiati += predicted
        cerchiati += (1 - predicted)
    print "in folder " + folder
    print "Predicted regular " + str(cerchiati)
    print "Predicted jellyfish " + str(noncerchiati)

predict_on_folder('test/zero')
predict_on_folder('test/five')

# Neural Network Classifier for finding Jellyfish Galaxies in the Illustris TNG simulation

## What is it?
Simple NN classifier for a project I was currently working on with KiYun Yun and Annalisa Pillepich (MPIA Heidelberg). Can be easily adapted to any classification problem. How to use instructions and input image format, etc. are tailored to this specific application.

## How to use
You need to make a folder train and a folder test.
Then make train/zero and train/five and put 500 images from category 0 in train/zero, 500 images from category 5 in train/five
Then put 99 images from category 0 in test/zero and 99 images from category 5 in test/five
The images you put in test and in train must be different.
You will be left with 0 images from category 0, but with some images from category 5

run python train.py

It will show you training progress, it should take not more than 20 minutes on a decent GPU (I train with a Titan V).
In training you see four numbers: loss, accuracy, val. loss, val. acc.
At the start accuracy is about 0.5, at the end should be about 0.9, val. loss at the start is also about 0.5, at the end gets to about 0.8.
At the end the trained model is saved in model.hd5 file

If you run python predict.py it uses that model to predict the type of
the images in test/zero (most should turn out to be regular galaxies, e.g. you can get 72 regular, 27 jellyfish) and in test/five (most will turn out to be jellyfish)

If you use the pre-trained model that I saved in model.hd5 you can run predict.py without doing the training first.


#!/usr/bin/python

import keras
import os
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import matplotlib.image as mpimg

#width and height of input images in pixel
width = 800
height = 800

#reading in files from train folder
def list_train_files():
    train_files = []
    for dirname, dirnames, filenames in os.walk('train'):
        for filename in filenames:
            train_files.append(os.path.join(dirname, filename))
    return(train_files)

#reading in files from test folder
def list_test_files():
    test_files = []
    for dirname, dirnames, filenames in os.walk('test'):
        for filename in filenames:
            test_files.append(os.path.join(dirname, filename))
    return(test_files)

#read an image
def load_image(x):
    img = mpimg.imread(x)
    img = img[:,:,0] #keep only red channel
    return(img)

#filenames encode jellyfish status, turn that into binary label
def load_label(x):
    if "zero" in x:
        return(0)
    else:
        return(1)

#read in all images and display some diagnostics, reshape into train/test matrices
def hst_load_data():
    train_files = list_train_files()
    test_files = list_test_files()
    print "Loading training images..."
    x_train = np.array([load_image(x) for x in train_files])
    y_train = np.array([load_label(x) for x in train_files])
    print "Loading test images..."
    x_test = np.array([load_image(x) for x in test_files])
    y_test = np.array([load_label(x) for x in test_files])

    n_train = len(train_files)
    n_test = len(test_files)
    print "Loaded " + str(n_train) + " training images, " + str(n_test) + " test images."

    train_shape = (n_train, width, height, 1)
    test_shape = (n_test, width, height, 1)
    
    x_train = x_train.reshape(train_shape)
    x_test = x_test.reshape(test_shape)
    y_train = y_train.reshape(n_train, )
    y_test = y_test.reshape(n_test, )

    print "Total number of images " + str(n_train + n_test)
    print "Number of regular galaxies in train " + str(len(y_train) - y_train.sum())
    print "Number of jellyfish galaxies in test " + str(len(y_test) - y_test.sum())

    return (x_train, y_train), (x_test, y_test)

#actually read the data
(x_train, y_train), (x_test, y_test) = hst_load_data()

#check that shapes are ok
print x_train.shape
print y_train.shape
print x_test.shape
print y_test.shape
print x_train.mean()

#set training parameters
batch_size = 10
num_classes = 2
epochs = 75

#convert to 1-hot encoding
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

print x_train.shape
print y_train.shape

#set up the neural network
model = Sequential()
eta_dropout = 0.35 #dropout value can be changed e.g. by hyperpar optim. loop

#convolutional layer
model.add(Conv2D(32, (3, 3), padding='same',
                 input_shape=x_train.shape[1:]))
model.add(Activation('relu'))
model.add(Dropout(eta_dropout)) #dropout
model.add(MaxPooling2D(pool_size=(2, 2))) #max pooling

#convolutional layer, same as before
model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(Dropout(eta_dropout))
model.add(MaxPooling2D(pool_size=(2, 2)))

#convolutional layer, same as before
model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(Dropout(eta_dropout))
model.add(MaxPooling2D(pool_size=(2, 2)))

#convolutional layer, same as before
model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(Dropout(eta_dropout))
model.add(MaxPooling2D(pool_size=(2, 2)))

#fully connected layer
model.add(Flatten())
model.add(Dense(32))
model.add(Activation('relu'))
model.add(Dropout(eta_dropout)) #with dropout

#fully connected layer
model.add(Dense(32))
model.add(Activation('relu'))
model.add(Dropout(eta_dropout))

#fully connected layer
model.add(Dense(32))
model.add(Activation('relu'))
model.add(Dropout(eta_dropout))

#last layer outputs the 'probability' of predicted classes
model.add(Dense(num_classes))
model.add(Activation('softmax'))

#lets have a look at the neural net
model.summary()

#rmsprop optimizer with learning rate and decay (try to change them)
opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)

#compile the model with binary crossentropy loss and accuracy metric
model.compile(loss='binary_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])

#make sure the train and test pixel values are floats
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')

#finally train the model
model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_test, y_test), shuffle=True)

#save it into an hdf5 file
model.save("model.hd5")
